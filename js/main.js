'use strict'
/* 
 Теоретичні питання
 1. Опишіть своїми словами, що таке метод об'єкту

 Це функції, які знаходяться в середині обєкта. 

 2. Який тип даних може мати значення властивості об'єкта?

 МОже мати будь-який тип даних - числа рядки, масиви, об'єкти, функції

 3. Об'єкт це посилальний тип даних. Що означає це поняття?

 Що при створенны об'єкта в зміну потрапляє не сам обект а посилання на нього. При копіюванні значення змінної ми просто копіюється посилання на об'єкт, а не сам об'єкт (об'єкт не клонується (дублюеться)).

 Практичні завдання
 1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.
 
 2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком, 
 наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік 
 за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.

3.Опціональне. Завдання:
Реалізувати повне клонування об'єкта. 

Технічні вимоги:
- Написати функцію для рекурсивного повного клонування об'єкта (без єдиної передачі за посиланням, внутрішня вкладеність властивостей об'єкта може бути досить великою).
- Функція має успішно копіювати властивості у вигляді об'єктів та масивів на будь-якому рівні вкладеності.
- У коді не можна використовувати вбудовані механізми клонування, такі як функція Object.assign() або spread.
 */

// Задача 1

// const product = {
//   name: 'Пица',
//   price: 1200,
//   discount: 100,
//   getNewPrice() {
//     return this.price - this.discount
//   },
// }
// console.log(product.getNewPrice())

// Задача 2

// let name = prompt('Введіть Ваше імя')
// let age = prompt('Скільки Вам років')

// while (true) {
//   if (name !== null && name !== '' && isNaN(name)) {
//     break
//   } else {
//     name = prompt('Все таки - введіть Ваше імя')

//     if (name !== null && name !== '' && isNaN(name)) {
//       break
//     }
//   }
// }

// while (true) {
//   if (age !== null && age !== '' && !isNaN(age) && +age !== 0) {
//     break
//   } else {
//     age = prompt('Все таки - скільки Вам років')

//     if (age !== null && age !== '' && !isNaN(age) && +age !== 0) {
//       break
//     }
//   }
// }

// const person = {
//   name,
//   age,
// }

// function returnHello(obj) {
//   alert(`Привіт я ${obj.name}, мені ${obj.age} років`)
// }

// returnHello(person)

// Задача 3

// const person = {
//   name: 'John',
//   surname: 'Blackwood',
//   job: 'carpenter',

//   family: {
//     count: 3,
//     members: {
//       family3: {
//         mother: { name: 'Сара' },
//         father: { name: 'Иван' },
//         array: ['Сара', { name: 'Джуана' }, ['Сара', { name: 'Джуана' }]],
//       },
//       father: {
//         name: 'Иван',
//         array: ['Сара', { name: 'Джуана' }],
//       },
//       sister: { name: 'Джуана' },
//     },
//   },
//   family2: {
//     array: ['Сара', { name: 'Джуана' }],
//   },
// }

// function cloneObject(cloneObj, obj) {
//   for (let key in obj) {
//     if (typeof obj[key] === 'object' && obj[key] !== null) {
//       if (!Array.isArray(obj[key])) {
//         cloneObj[key] = Object.create(obj[key])
//         cloneObject(cloneObj[key], obj[key])
//       } else {
//         cloneObj[key] = new Array(...obj[key])
//         cloneObject(cloneObj[key], obj[key])
//       }
//     } else {
//       cloneObj[key] = obj[key]
//     }
//   }
// }
// const person2 = {}
// cloneObject(person2, person)

// console.log(person)

// console.log(person2)

// person2.family.members.father.name = 'Петро'
// person2.family.members.family3.mother.name = 'Марьяна'
// person2.family.members.sister.name = 'Тетяна'
// person2.family.count = 8
// person2.family2.array[0] = 'Мария'
// person2.family2.array[1].name = 'Оксана'
// person2.family.members.family3.array[1].name = 'Полина'
